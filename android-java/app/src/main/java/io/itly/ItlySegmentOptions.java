package io.itly;

public class ItlySegmentOptions implements IItlyAdapterOptions {
    private android.content.Context androidContext;

    public ItlySegmentOptions(android.content.Context androidContext) {
        this.androidContext = androidContext;
    }

    public android.content.Context getAndroidContext() {
        return this.androidContext;
    }
}
