from datetime import datetime

import itly


class CustomDestination(itly.Destination):
    def init(self):
        # type: () -> None
        print("SampleCustomAdapter.init()")

    def alias(self, user_id, previous_id, timestamp):
        # type: (str, str, datetime) -> None
        print("SampleCustomAdapter.alias(userId: {0}, previousId: {1})".format(user_id, previous_id))

    def identify(self, user_id, properties, timestamp):
        # type: (str, itly.Properties, datetime) -> None
        print("SampleCustomAdapter.identify(userId: {0}, properties: {1})".format(
            user_id, properties.to_json()))

    def group(self, user_id, group_id, properties, timestamp):
        # type: (str, str, itly.Properties, datetime) -> None
        print("SampleCustomAdapter.group(user_id: {0}, groupId: {1}, properties: {2})".format(
            user_id, group_id, properties.to_json()))

    def track(self, user_id, event_name, properties, timestamp):
        # type: (str, str, itly.Properties, datetime) -> None
        print("SampleCustomAdapter.track(eventName: {0}, userId: {1}, properties: {2})".format(
            event_name, user_id, properties.to_json()))
