/* eslint-disable */
import itly from '@itly/sdk';
import SchemaValidatorPlugin from '@itly/plugin-schema-validator';
import IterativelyPlugin from '@itly/plugin-iteratively';
import AmplitudePlugin from '@itly/plugin-amplitude';
import MixpanelPlugin from '@itly/plugin-mixpanel';
import SegmentPlugin from '@itly/plugin-segment';

class SendEventButtonClicked {
  constructor() {
    this.name = 'Send Event Button Clicked';
    this.id = '5c2c7b63-6161-4f70-b6ef-0fec926fc2c4';
    this.version = '2.0.0';
  }
}

class AlertButtonClicked {
  constructor(properties) {
    this.name = 'Alert Button Clicked';
    this.id = 'ebee3afd-40a0-44c2-a86e-ff16577d698f';
    this.version = '1.0.0';
    this.properties = properties;
  }
}

// prettier-ignore
class Itly {
  /*
   * Initialize the Itly SDK. Call once when your application starts.
   * @param {Object} options Configuration options to initialize the Itly SDK with.
   * @param {Object} options.context Set of properties to add to every event.
   * @param {boolean} [options.disabled=false] Flag to completely disable the Itly SDK.
   * @param {string} [options.environment=development] The environment the Itly SDK is running in (development or production).
   * @param {Object} [options.plugins] Collection of Plugin's.
   * @param {Object} [options.destinations] Analytics provider-specific configuration.
   */
  load(options) {
    if (!options.context) {
      throw new Error('Your tracking plan contains at least one context property but a `context` object was not provided on `options`.');
    }

    const {
      destinations = {},
      plugins = [],
      ...baseOptions
    } = options;

    if (!options.plugins || !options.plugins.length > 0) {
      throw new Error('Your tracking plan is configured with a custom destination but a `plugins` array was not provided on `options`.');
    }


    const destinationPlugins = destinations.all && destinations.all.disabled
      ? []
      : [
        new IterativelyPlugin(options.environment === 'production'
          ? 'xZbUv_ZUKol2fhsP68SLjzOGegOUohhx'
          : 'JJzXhD3V1qtd11tO7owVVKURzTcvUZ37',
          {
            url: 'https://api.iterative.ly/t/version/9616e203-e3cc-44fa-9338-84f61eb78f4d',
            environment: options.environment || 'development',
            ...destinations.iteratively,
          },
        ),
        new AmplitudePlugin(options.environment === 'production'
          ? '6c3a9cd0a22daafe3278346608a816cf'
          : '6c3a9cd0a22daafe3278346608a816cf',
          destinations.amplitude,
        ),
        new MixpanelPlugin(options.environment === 'production'
          ? '67a8ece0a81e35124d7c23c06b04c52f'
          : '67a8ece0a81e35124d7c23c06b04c52f',
          destinations.mixpanel,
        ),
        new SegmentPlugin(options.environment === 'production'
          ? 'vTHA3Cg3K6BVBsRZasHj45glQj0Br2c0'
          : 'vTHA3Cg3K6BVBsRZasHj45glQj0Br2c0',
          destinations.segment,
        ),
      ];

    itly.load({
      ...baseOptions,
      plugins: [
        new SchemaValidatorPlugin({
          'context': {"$id":"https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/context","$schema":"http://json-schema.org/draft-07/schema#","title":"Context","description":"","type":"object","properties":{"version":{"description":"","type":"string"}},"additionalProperties":false,"required":["version"]},
          'identify': {"$id":"https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/identify","$schema":"http://json-schema.org/draft-07/schema#","title":"Identify","description":"","type":"object","properties":{"lastName":{"description":"The user's last name.","type":"string"},"firstName":{"description":"The user's first name.","type":"string"}},"additionalProperties":false,"required":["lastName","firstName"]},
          'Alert Button Clicked': {"$id":"https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/event/Alert%20Button%20Clicked/version/1.0.0","$schema":"http://json-schema.org/draft-07/schema#","title":"Alert Button Clicked","description":"Called when the user clicks the Alert button.","type":"object","properties":{"alertKind":{"description":"The kind of the alert.","enum":["yellow","red"]}},"additionalProperties":false,"required":["alertKind"]},
          'Send Event Button Clicked': {"$id":"https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/event/Send%20Event%20Button%20Clicked/version/2.0.0","$schema":"http://json-schema.org/draft-07/schema#","title":"Send Event Button Clicked","description":"Called when the user clicks the Send Event button.","type":"object","properties":{},"additionalProperties":false,"required":[]},
        }),
        ...destinationPlugins,
        ...plugins,
      ]
    });
  }

  /**
   * Alias a user ID to another user ID.
   * @param {string} userId The user's new ID.
   * @param {string} previousId The user's previous ID.
   */
  alias(userId, previousId) {
    itly.alias(userId, previousId);
  }

  /**
   * Identify a user and set or update that user's properties.
   * @param {string} [userId] The user's ID.
   * @param {Object} properties The user's properties.
   * @param {string} properties.lastName The user's last name.
   * @param {string} properties.firstName The user's first name.
   */
  identify(userId, properties) {
    if (Object.prototype.toString.call(userId) === '[object Object]') {
      properties = userId;
      userId = undefined;
    }

    if (!properties) {
      throw new Error('Your tracking plan contains at least one user property but `properties` were not passed as an argument.');
    }
    itly.identify(userId, properties);
  }

  /**
   * Associate the current user with a group.
   * @param {string} groupId The group's ID.
   */
  group(groupId) {
    itly.group(groupId);
  }

  /**
   * Track a page view.
   * @param {string} category The page's category.
   * @param {string} name The page's name.
   */
  page(category, name) {
    itly.page(category, name);
  }

  /**
   * Called when the user clicks the Send Event button.
   * 
   * Owner: Ondrej Hrebicek (no SSO)
   */
  sendEventButtonClicked() {
    itly.track(new SendEventButtonClicked());
  }

  /**
   * Called when the user clicks the Alert button.
   * 
   * Owner: Ondrej Hrebicek (no SSO)
   * @param {Object} properties The event's properties.
   * @param {string} properties.alertKind The kind of the alert.
   */
  alertButtonClicked(properties) {
    if (!properties) {
      throw new Error('There is at least one property defined on this event in your tracking plan but `properties` were not passed as an argument.');
    }

    itly.track(new AlertButtonClicked(properties));
  }

  track(event) {
    itly.track(event);
  }

  reset() {
    itly.reset();
  }
}

const itlySdk = new Itly();

export default itlySdk;
export { AlertButtonClicked, SendEventButtonClicked };
