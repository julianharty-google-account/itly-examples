package io.itly;

import com.segment.analytics.Analytics;
import com.segment.analytics.messages.AliasMessage;
import com.segment.analytics.messages.GroupMessage;
import com.segment.analytics.messages.IdentifyMessage;
import com.segment.analytics.messages.TrackMessage;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ItlySegmentAdapter implements IItlyDestination {
    private ItlyOptions options;
    private Analytics client;

    ItlySegmentAdapter(ItlyOptions options, String apiToken) {
        this.options = options;
        this.client = Analytics.builder(apiToken).build();
    }

    public void init() {
        // N/A for Segment
    }

    public void alias(String userId, String previousId) {
        this.client.enqueue(AliasMessage.builder(previousId)
            .userId(userId));
    }

    public void identify(String userId, ItlyPropertyMap identifyProperties) {
        HashMap<String, Object> traits = null;

        if (identifyProperties != null && identifyProperties.getSize() > 0) {
            traits = new HashMap<>();

            Iterator<Map.Entry<String, Object>> entries = identifyProperties.getEntrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry<String, Object> property = entries.next();
                traits.put(property.getKey(), property.getValue());
            }
        }

        if (traits == null) {
            this.client.enqueue(IdentifyMessage.builder()
                .userId(userId)
            );
        }
        else {
            this.client.enqueue(IdentifyMessage.builder()
                .userId(userId)
                .traits(traits)
            );
        }
    }

    public void group(String userId, String groupId, ItlyPropertyMap groupProperties) {
        HashMap<String, Object> traits = null;

        if (groupProperties != null && groupProperties.getSize() > 0) {
            traits = new HashMap<>();

            Iterator<Map.Entry<String, Object>> entries = groupProperties.getEntrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry<String, Object> property = entries.next();
                traits.put(property.getKey(), property.getValue());
            }
        }

        if (traits == null) {
            this.client.enqueue(GroupMessage.builder(groupId)
                .userId(userId)
            );
        }
        else {
            this.client.enqueue(GroupMessage.builder(groupId)
                .userId(userId)
                .traits(traits)
            );
        }
    }

    public void track(String userId, String eventName, ItlyPropertyMap eventProperties) {
        HashMap<String, Object> properties = null;

        if (eventProperties != null && eventProperties.getSize() > 0) {
            properties = new HashMap<>();

            Iterator<Map.Entry<String, Object>> entries = eventProperties.getEntrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry<String, Object> property = entries.next();
                properties.put(property.getKey(), property.getValue());
            }
        }

        if (properties == null) {
            this.client.enqueue(TrackMessage.builder(eventName)
                    .userId(userId)
            );
        }
        else {
            this.client.enqueue(TrackMessage.builder(eventName)
                    .userId(userId)
                    .properties(properties)
            );
        }
    }

    public void reset() {
        // N/A for Segment
    }
}
