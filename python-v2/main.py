import multiprocessing
import itly

from logger import SampleLogger
from custom import CustomDestination

print("Starting process...")

itly.load(
    context=itly.Context(
        version="1.0",
    ),
    options=itly.Options(
        logger=SampleLogger(),
        plugins=[
            CustomDestination()
        ],
    ),
)

itly.process_started("some-user-id",
    available_processors=multiprocessing.cpu_count()
)
itly.track("some-user-id", itly.ProcessStarted(
    available_processors=multiprocessing.cpu_count()
))

print("Ending process...")

itly.flush()
itly.shutdown()
