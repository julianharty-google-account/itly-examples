//
//  CustomDestination.swift
//  IterativelySwift
//
//  Created by Ondrej Hrebicek on 11/5/20.
//  Copyright © 2020 Iteratively Inc. All rights reserved.
//

import Foundation

final class CustomDestination: ItlyDestination {
    func identify(_ userId: String?, properties: [String: Any]?) {
        let value  = propertiesValue(properties: properties)
        print(String(format: "CustomDestination.identify(userId: \(userId ?? "nil"), properies \(value)"))
    }

    func alias(_ userId: String, previousId: String?) {
        print(String(format: "CustomDestination.alias(userId: \(userId), previousId: \(previousId ?? "nil")"))
    }

    func group(_ groupId: String, properties: [String: Any]?) {
        let value = propertiesValue(properties: properties)
        print(String(format: "CustomDestination.group(groupId: \(groupId), properties: \(value)"))
    }

    func track(_ userId: String?, eventName: String, properties: [String: Any]) {
        let value = propertiesValue(properties: properties)
        print(String(format: "CustomDestination.track(eventName: \(eventName), properties: \(value)"))
    }

    func reset() {
        print("CustomDestination.reset()")
    }

    func propertiesValue(properties: [String: Any]?) -> String {
        var propertiesValue = "None"
        if (properties != nil) {
            propertiesValue = properties!.keys.joined()
        }
        return propertiesValue
    }
}
