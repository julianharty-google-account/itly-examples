require_relative 'itly/itly'
require_relative 'custom_plugin'

logger = Itly::Loggers.std_out

Itly.load(
  context: Itly::Context.new(
    required_string: 'Ruby context string',
    optional_enum: Itly::Context::OptionalEnum::VALUE_1
  ),
  destinations: Itly::DestinationOptions.new(
    iteratively: Itly::Plugin::Iteratively::Options.new(
      batch_size: 1,
      max_retries: 2
    ),
    snowplow: Itly::Plugin::Snowplow::Options.new(
      endpoint: 'sp-dev.iterative.ly'
    ),
  ),
  options: Itly::Options.new(
    plugins: [CustomPlugin.new(api_key: 'abc123')],
    validation: Itly::Options::Validation::ERROR_ON_INVALID,
    logger: logger
  )
)

user_id = 'user-id'

Itly.identify('tmp-user-id', required_number: 42)

Itly.alias(user_id, 'tmp-user-id')
Itly.group(user_id, 'group-id', required_boolean: true)

# Strongly typed methods for each Event in your tracking plan
Itly.user_signed_in(user_id, method: Itly::UserSignedIn::Method::SSO)
Itly.song_played(user_id,
    title: "Beatles - Let It Be",
    duration: 246,
    genre: Itly::SongPlayed::Genre::GOOD_MUSIC)

Itly.song_played(user_id,
    title: 'Garth Brooks - Friends in Low Places',
    duration: 340,
    genre: Itly::SongPlayed::Genre::COUNTRY)

Itly.user_signed_out(user_id, active: true)

second_user_id = 'user-2'
Itly.identify(second_user_id, required_number: 43)

# Event classes can also be used with the track() method
Itly.track(second_user_id, Itly::UserSignedIn.new(
    method: Itly::UserSignedIn::Method::EMAIL
))

# Itly.track() allows for options to be passed to the underlying destinations
Itly.track(second_user_id, Itly::SongPlayed.new(
    title: 'Prince - Purple Rain',
    duration: 841
), options: Itly::TrackOptions.new(
  amplitude: Itly::Plugin::Amplitude::TrackOptions.new(
    platform: 'Ruby',
    country: 'United States',
    region: 'California',
    city: 'San Diego',
    device_brand: 'Apple',
    device_model: 'MacBook Pro',
    os_name: 'OSX',
    os_version: 'Big Sur',
    language: 'English',
    callback: ->(status, body) { puts "Amplitude complete. Response: #{status} => #{body}" },

    # NOTE: insert_id must be unique per call
    # WARNING: If insert_id is set to a fixed value events will fail appear more than once
    # insert_id: 'f47ac10b-58cc-4372-a567-0e02b2c3d901',
  ),
  segment: Itly::Plugin::Segment::TrackOptions.new(
    integrations: { 'Google Analytics': false },
    anonymous_id: 'segment-anonymous-id',
    context: { 'contextProp' => true },
    callback:  ->(status, body) { puts "Segment complete. Response: #{status} => #{body}" },

    # warning: message_id, timestamp aren't supported by Segment client at this time
    # message_id: 'segment-message-id',
    # timestamp: '2021-06-04T00:32:28.781Z'
  ),
  snowplow: Itly::Plugin::Snowplow::TrackOptions.new(
    contexts: [
       Itly::Plugin::Snowplow::Context.new(schema: 'schema-id', data: { 'snowplowContextProp' => true })
    ],
    # warning: Snowplow does not support a completion callback at this time
    # callback -> Not Supported
  )
))

Itly.user_signed_out(second_user_id)

Itly.flush

Itly.shutdown
