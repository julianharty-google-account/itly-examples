import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import itly, { /* Loggers */ } from './itly';
import CustomDestination from './CustomDestination';

itly.load({
  /* The tracking plan requires a set of properties to be included on every event; we set them here. */
  context: { version: '0.0.1' },
  /* Uncomment this line to log Itly SDK debug information to the console. */
  // logger: Loggers.Console,
  /* The environment - 'development' or 'production' - determines which access token to use and what to do when an event fails validation (throw an error or still track). */
  environment: 'development',
  /* The tracking plan requires a custom destination; we specify it here. */
  plugins: [new CustomDestination()],
});

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
