//
//  FirstViewController.swift
//  IterativelySwift
//
//  Created by Ondrej Hrebicek on 11/27/19.
//  Copyright © 2019 Iteratively Inc. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        Itly.instance.trackViewLoaded(ViewLoaded(name: .firstView))
    }


}

