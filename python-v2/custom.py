from datetime import datetime
from typing import Optional

import itly

class CustomDestination(itly.Plugin):
    def id(self) -> str:
        return "custom-destination"

    def load(self, options: itly.PluginLoadOptions) -> None:
        print("load...")
        pass

    def alias(self, user_id: str, previous_id: str) -> None:
        print("alias...")
        pass

    def identify(self, user_id: str, properties: Optional[itly.Properties]) -> None:
        print("identify...")
        pass

    def group(self, user_id: str, group_id: str, properties: Optional[itly.Properties]) -> None:
        print("group...")
        pass

    def page(self, user_id: str, category: Optional[str], name: Optional[str], properties: Optional[itly.Properties]) -> None:
        print("page...")
        pass

    def track(self, user_id: str, event: itly.Event) -> None:
        print("track... (name: {0}, properties: {1})".format(event.name, event.properties))
        pass

    def flush(self) -> None:
        print("flush...")
        pass

    def shutdown(self) -> None:
        print("shutdown...")
        pass
