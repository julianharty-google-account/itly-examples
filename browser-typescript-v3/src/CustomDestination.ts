import { Plugin, Options, Event, PluginCallOptions } from './itly';

export interface CustomTrackOptions extends PluginCallOptions {
  awesome: boolean;
}

export default class extends Plugin {
  constructor() {
    super('custom-destination');
  }

  load(options: Options) {
    console.log(`Custom Destination: load()`, options);
  }

  track(_: string, event: Event, options?: CustomTrackOptions) {
    console.log(`Custom Destination: track()`, event, options);
    console.log(`Custom Destination: awesome: ${options?.awesome}`);
  }
}
