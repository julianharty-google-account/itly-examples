# Iteratively Ruby (2.0) Rails Sample Application

## Overview
This project is an example Ruby on Rails application using the strongly typed Ruby SDK generated
by Iteratively via `itly pull <ruby-v2-source>` for a Tracking Plan with the following Events:

* User Signed In
* User Signed Out
* Song Played

## REQUIRED CHANGES
By default, this example only validates and logs Events to standard output.

If you want to see events in included destinations such as Amplitude, Mixpanel, Segment, Snowplow
you need to update the `load()` method in `vendor/itly.rb`.

1. Uncomment the Plugins you want to activate
2. Set a valid API key for the given Plugin


## Project Files

* ruby-v2-rails
    * config/initializer/iteratively.rb - Sets up the Itly SDK
    * vendor/itly.rb - The code generated Itly SDK for an example tracking plan. Created via `itly pull <ruby-v2-source> --path "./vendor"`.
    * Gemfile - Project dependencies
    * README.md - You are here *

# Setup

## Install a Ruby

You can install Ruby through your distribution's packages or through a Ruby Manager.

## Set up Ruby on Rails
* https://guides.rubyonrails.org/getting_started.html

## Install the gems

Then install the bundler gem:

    gem install bundler

And install the itly gems and their dependencies:

    cd /ruby-v2-rails
    bundle install

## Run

To run the sample app:

    cd /ruby-v2-rails
    bin/rails server
