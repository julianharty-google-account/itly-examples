import React from 'react';
import logo from './logo.svg';
import './App.css';
import itly from './itly';
import { CustomTrackOptions } from './CustomDestination';

const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload...
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <button onClick={() => {
          itly.buttonClicked(
            /* event's properties */
            { label: 'Send Event' },
            /* track options for custom-destination plugin */
            { 'custom-destination': { awesome: true } as CustomTrackOptions },
          )
        }}>
          Send Event
        </button>
      </header>
    </div>
  );
}

export default App;
