package io.itly;

import java.util.HashMap;

public class ProcessStarted extends ItlyPropertyMap {
    private ProcessStarted() {}

    private ProcessStarted(Builder builder) {
        this.properties.putAll(builder.properties.properties);
    }

    private ProcessStarted(ItlyPropertyMap clone) {
        this.properties.putAll((HashMap<String, Object>) clone.properties.clone());
    }

    public ProcessStarted clone() {
        return new ProcessStarted(this);
    }

    public static IAvailableProcessors builder() { return new Builder(); }

    // Inner Builder class with required properties
    public static class Builder implements IAvailableProcessors, IBuild {
        ProcessStarted properties = new ProcessStarted();

        private Builder() {}

        /**
         * Number of CPUs.
         * <p>
         * Must be followed by by additional optional properties or build() method
         */
        public Builder availableProcessors(Integer availableProcessors) {
            this.properties.properties.put("availableProcessors", availableProcessors);
            return this;
        }

        public ProcessStarted build() {
            return new ProcessStarted(this);
        }
    }

    // Required property interfaces
    public interface IAvailableProcessors {
        Builder availableProcessors(Integer availableProcessors);
    }

    /** Build interface with optional properties */
    public interface IBuild {
        ProcessStarted build();
    }
}
