from __future__ import print_function
import sys

import itly


class SampleLogger(itly.Logger):
    def debug(self, message):
        # type: (str) -> None
        print(message)

    def info(self, message):
        # type: (str) -> None
        print(message)

    def warn(self, message):
        # type: (str) -> None
        print(message)

    def error(self, message):
        # type: (str) -> None
        print(message, file=sys.stderr)