//
//  SceneDelegate.h
//  ItlyObjCCarthageExample
//
//  Created by Ondrej Hrebicek on 11/10/20.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

