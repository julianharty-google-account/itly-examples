package io.itly;

public class ItlyCustomOptions implements IItlyAdapterOptions {
    private IItlyDestination adapter;

    public ItlyCustomOptions(IItlyDestination adapter) {
        this.adapter = adapter;
    }

    public IItlyDestination getAdapter() {
        return this.adapter;
    }
}
