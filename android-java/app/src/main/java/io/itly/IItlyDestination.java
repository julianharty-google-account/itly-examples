package io.itly;

public interface IItlyDestination {
  void init();
  void alias(String userId, String previousId);
  void identify(String userId, ItlyPropertyMap properties);
  void group(String groupId, ItlyPropertyMap properties);
  void track(String userId, String eventName, ItlyPropertyMap properties);
  void reset();
}
