require 'etc'
require 'itly'

puts "Starting process..."

Itly.load do |options|
    options.context = Itly::Context.new(
        version: '1.0'
    )
end

Itly.identify('some-user-id', first_name: 'John', last_name: 'Doe')
Itly.process_started('some-user-id', available_processors: Etc.nprocessors)
Itly.track('some-user-id', Itly::ProcessStarted.new(
    available_processors: Etc.nprocessors
))

puts "Ending process..."
