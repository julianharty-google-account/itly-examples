package ly.iterative.examples.kotlin

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem

import kotlinx.android.synthetic.main.activity_main.*
import ly.iterative.itly.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        Itly.load(
            Context(version = "1.0"),
            DestinationsOptions(
                AmplitudeOptions(applicationContext),
                MixpanelOptions(applicationContext),
                SegmentOptions(applicationContext)
            ),
            Options(
                plugins = listOf(MyCustomDestination())
            )
        );

        Itly.activityCreated(
            title = ActivityCreated.Title.MAIN_ACTIVITY
        )

        fab.setOnClickListener { view ->
            Itly.mailButtonClicked()
            Itly.snackbarDisplayed(
                length = Snackbar.LENGTH_LONG,
                text = "Replace with your own action"
            )
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        Itly.optionsOpened()
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        Itly.optionsItemSelected(
            id = item.itemId,
            title = item.toString()
        )
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when(item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
