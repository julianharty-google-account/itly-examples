//
//  AppDelegate.h
//  ItlyObjCCarthageExample
//
//  Created by Ondrej Hrebicek on 11/10/20.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@end

