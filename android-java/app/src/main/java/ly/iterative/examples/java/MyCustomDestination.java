package ly.iterative.examples.java;

import io.itly.*;

public class MyCustomDestination implements IItlyDestination {
    public void init() {
        System.out.println("MyCustomDestination.init()");
    }

    public void alias(String userId, String previousId) {
        System.out.println(String.format("MyCustomDestination.alias(userId: %s, previousId: %s)", userId, previousId));
    }

    public void identify(String userId, ItlyPropertyMap properties) {
        System.out.println(String.format("MyCustomDestination.identify(userId: %s, properties: %s)", userId, (properties == null) ? "null": properties.toJson().toString()));
    }

    public void group(String groupId, ItlyPropertyMap properties) {
        System.out.println(String.format("MyCustomDestination.group(groupId: %s, properties: %s)", groupId, (properties == null) ? "null": properties.toJson().toString()));
    }
    public void reset() {
        System.out.println("MyCustomDestination.reset()");
    }

    public void track(String userId, String eventName, ItlyPropertyMap properties) {
        System.out.println(String.format("MyCustomDestination.track(eventName: %s, userId: %s, properties: %s)", eventName, userId, (properties == null) ? "null": properties.toJson().toString()));
    }
}
