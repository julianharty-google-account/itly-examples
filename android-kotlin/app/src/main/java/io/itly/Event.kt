//
//  Event.kt
//  This file is auto-generated by Iteratively. Run `itly pull kotlin` to update.
//

package io.itly

abstract class Event(
    val name: String,
    val schema: String,
    properties: Map<String, Any?>
) : Properties(
    properties
)