//
//  CustomLogger.m
//  ItlyObjCCarthageExample
//
//  Created by Ondrej Hrebicek on 11/12/20.
//

#import "CustomLogger.h"

@implementation CustomLogger
- (void)debug:(NSString * _Nonnull)message {
    NSLog(@"debug: %@", message);
}

- (void)info:(NSString * _Nonnull)message {
    NSLog(@"info: %@", message);
}

- (void)warn:(NSString * _Nonnull)message {
    NSLog(@"warn: %@", message);
}

- (void)error:(NSString * _Nonnull)message {
    NSLog(@"error: %@", message);
}
@end
