package io.itly;

import java.util.*;
import java.lang.*;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONException;
import org.json.JSONObject;

public class Itly {
    private ItlyOptions options;
    private ArrayList<IItlyDestination> adapters = new ArrayList<IItlyDestination>();
    private static volatile Itly singleton = null;

    private static final Map<String, JSONObject> schemaValidators = new HashMap<String, JSONObject>();

    private Itly() {
    }

    /**
     * Initializes Itly with configured destinations
     * @param options Configuration for Itly instance.
     */
    public void init(ItlyOptions options) {
        if (this.options != null) {
            throw new Error("Itly is already initialized.");
        }

        this.options = options;

        this.adapters.add(options.destinations.custom.getAdapter());
        this.adapters.add(new ItlySegmentAdapter(options, this.options.environment == ItlyOptions.Environment.PRODUCTION ? "CvU1TXIk6zSKuN7Vyx3FMT7cqzyvY5th" : "CvU1TXIk6zSKuN7Vyx3FMT7cqzyvY5th"));

        for (IItlyDestination adapter : this.adapters) {
            adapter.init();
        }
    }

    /**
     * Returns singleton instance of Itly
     */
    public static Itly getInstance() {
        if (singleton == null) {
            createSingleton();
        }
        return singleton;
    }

    private synchronized static void createSingleton() {
        if (singleton == null) {
            singleton = new Itly();
            try {
                schemaValidators.put("Context", new JSONObject("{\"$id\":\"https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/context\",\"$schema\":\"http://json-schema.org/draft-07/schema#\",\"title\":\"Context\",\"description\":\"\",\"type\":\"object\",\"properties\":{\"version\":{\"description\":\"\",\"type\":\"string\"}},\"additionalProperties\":false,\"required\":[\"version\"]}"));
                schemaValidators.put("Identify", new JSONObject("{\"$id\":\"https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/identify\",\"$schema\":\"http://json-schema.org/draft-07/schema#\",\"title\":\"Identify\",\"description\":\"\",\"type\":\"object\",\"properties\":{\"firstName\":{\"description\":\"The user's first name.\",\"type\":\"string\"},\"lastName\":{\"description\":\"The user's last name.\",\"type\":\"string\"}},\"additionalProperties\":false,\"required\":[\"firstName\",\"lastName\"]}"));
                schemaValidators.put("ProcessStarted", new JSONObject("{\"$id\":\"https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/event/239f7db9-20ad-479c-a1e1-563c5af27938\",\"$schema\":\"http://json-schema.org/draft-07/schema#\",\"title\":\"Process Started\",\"description\":\"Called when the process starts.\",\"type\":\"object\",\"properties\":{\"availableProcessors\":{\"description\":\"Number of CPUs.\",\"type\":\"integer\"}},\"additionalProperties\":false,\"required\":[\"availableProcessors\"]}"));
            } catch (JSONException e) {}
        }
    }

    /**
     * Alias a user ID to another user ID.
     * @param userId The user's new ID.
     * @param previousId The user's previous ID.
     */
    public void alias(String userId, String previousId) {
        if (userId == null) {
            throw new IllegalArgumentException("userId cannot be null");
        }

        if (!this.isInitializedAndEnabled()) {
            return;
        }

        for (IItlyDestination adapter : this.adapters) {
            adapter.alias(userId, previousId);
        }
    }

   /**
     * Set or update a user's properties.
     * @param userId The user's ID.
     * @param properties Required and optional user properties.
     */
    public void identify(String userId, Identify properties) {
        if (properties == null) {
            throw new IllegalArgumentException("properties cannot be null");
        }

        if (!this.isInitializedAndEnabled()) {
            return;
        }

        validateSchema(properties, "Identify");

        for (IItlyDestination adapter : this.adapters) {
            adapter.identify(userId, properties);
        }
    }

    /**
    * Set or update a group's properties.
    * @param groupId The group's ID.
    */
   public void group(String userId, String groupId) {
       if (!this.isInitializedAndEnabled()) {
           return;
       }

       for (IItlyDestination adapter : this.adapters) {
           adapter.group(userId, groupId, null);
       }
   }
    /**
     * Called when the process starts.
     * 
     * Owner: Ondrej Hrebicek (no SSO)
     * @param userId The user's ID
     * @param properties The event's properties (e.g. availableProcessors)
     */
    public void trackProcessStarted(String userId, ProcessStarted properties) {
        if (properties == null) {
            throw new IllegalArgumentException("properties cannot be null");
        }

        this.trackEvent(
            userId,
            "Process Started",
            properties,
            "https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/event/239f7db9-20ad-479c-a1e1-563c5af27938",
            "ProcessStarted"
        );
    }

    /**
     * Called when user is logged out.
     */
    public void reset() {
        if (!this.isInitializedAndEnabled()) {
            return;
        }

        for (IItlyDestination adapter : this.adapters) {
            adapter.reset();
        };
    }

    private void trackEvent(String userId, String eventName, ItlyPropertyMap properties, String itlySchema, String validatorKey) {
        if (!this.isInitializedAndEnabled()) {
            return;
        }

        validateSchema(properties, validatorKey);
        validateSchema(this.options.context, "Context");

        // If duplicate property name exists, last one wins. In order of priority: properties, then context, then itly properties
        ItlyPropertyMap combinedPropertyMap = new ItlyProperties(itlySchema).concat(new ItlyPropertyMap[] {this.options.context, properties});

        for (IItlyDestination adapter : this.adapters) {
            try {
                adapter.track(userId, eventName, combinedPropertyMap);
            } catch (Exception e) {
                options.logger.error(String.format("Exception thrown in '%s.trackEvent' for event '%s' with message '%s'", adapter.getClass().getName(), eventName, e.getMessage()));
            }
        }
    }

    private boolean isInitializedAndEnabled() throws Error {
        if (this.options == null) {
            throw new Error("Itly is not initialized.");
        }

        return !this.options.disabled;
    }

    private void validateSchema(ItlyPropertyMap properties, String validatorKey) {
        if (properties == null) {
            // All events with properties have had this checked.
            return;
        }

        try {
            // Code generator generates a SchemaValidator for every event and, if applicable, Context, Group, and Identify.
            SchemaLoader loader = SchemaLoader.builder()
                .schemaJson(Itly.schemaValidators.get(validatorKey))
                .draftV7Support()
                .build();
            Schema schema = loader.load().build();
            schema.validate(properties.toJson());
        }
        catch (ValidationException e) {
            StringBuilder errorsBuilder = new StringBuilder();
            for (String error : e.getAllMessages()) {
                errorsBuilder.append(error);
            }

            this.handleValidationError(String.format("Itly validation error: %s", errorsBuilder));
        }
    }

    private void handleValidationError(String message) {
        if (this.options.logger != null) {
            this.options.logger.error(message);
        } else {
            System.err.println(message);
        }

        if (this.options.environment != ItlyOptions.Environment.PRODUCTION) {
            throw new IllegalArgumentException(message);
        }
    }
}
