package io.itly;

public class ItlyOptions {
    public enum Environment { PRODUCTION, DEVELOPMENT }

    /**
     * The current environment (development or production). Default is production.
     */
    public Environment environment;
    /**
     * Whether calls to the Itly SDK should be no-ops. Default is false.
     */
    public boolean disabled;
    /**
     * Analytics provider-specific configuration. Default is null.
     */
    public ItlyDestinations destinations;
    /**
     * Additional context properties to add to all events. Set to object or an object resolver. Default is none.
     */
    public Context context;
    /**
     * Custom logger to use for debug, info, warn, and error messages. Default is console.
     */
    public ItlyLogger logger;

    private ItlyOptions(Builder builder) {
        this.environment = builder.environment;
        this.disabled = builder.disabled;
        this.destinations = builder.destinations;
        this.context = builder.context;
        this.logger = builder.logger;
    }

    public static IDestinations builder() {
        return new Builder();
    }

    public static class Builder implements IDestinations, IContext, IBuild {
        private Environment environment = Environment.PRODUCTION;
        private boolean disabled = false;
        private ItlyDestinations destinations;
        private Context context;
        private ItlyLogger logger;

        private Builder() {}

        public IContext destinations(ItlyDestinations destinations) {
            this.destinations = destinations;
            return this;
        }

        public Builder context(Context context) {
            this.context = context;
            return this;
        }

        public Builder environment(Environment environment) {
            this.environment = environment;
            return this;
        }

        public Builder disabled(boolean disabled) {
            this.disabled = disabled;
            return this;
        }

        public Builder logger(ItlyLogger logger) {
            this.logger = logger;
            return this;
        }

        public ItlyOptions build() {
            return new ItlyOptions(this);
        }
    }

    public interface IDestinations {
        IContext destinations(ItlyDestinations destinations);
    }

    public interface IContext {
        IBuild context(Context context);
    }

    public interface IBuild {
        IBuild environment(Environment environment);
        IBuild disabled(boolean disabled);
        IBuild logger(ItlyLogger logger);
        ItlyOptions build();
    }
}
