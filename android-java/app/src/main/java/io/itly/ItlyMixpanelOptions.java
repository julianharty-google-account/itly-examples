package io.itly;

public class ItlyMixpanelOptions implements IItlyAdapterOptions {
    private android.content.Context androidContext;

    public ItlyMixpanelOptions(android.content.Context androidContext) {
        this.androidContext = androidContext;
    }

    public android.content.Context getAndroidContext() {
        return this.androidContext;
    }
}
