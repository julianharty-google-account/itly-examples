package io.itly;

import java.util.HashMap;

public class ItlyProperties extends ItlyPropertyMap {
    public ItlyProperties(String itlySchema) {
        properties.put("itlySource", "java");
        properties.put("itlyVersion", "1.0");
        properties.put("itlySchema", itlySchema);
    }
    private ItlyProperties(ItlyPropertyMap clone) {
        this.properties.putAll((HashMap<String, Object>) clone.properties.clone());
    }

    public ItlyProperties clone() {
        return new ItlyProperties(this);
    }
}
