package io.itly;

import java.util.HashMap;

public class Context extends ItlyPropertyMap {
    private Context() {}

    private Context(Builder builder) {
        this.properties.putAll(builder.properties.properties);
    }

    private Context(ItlyPropertyMap clone) {
        this.properties.putAll((HashMap<String, Object>) clone.properties.clone());
    }

    public Context clone() {
        return new Context(this);
    }

    public static IVersion builder() { return new Builder(); }

    // Inner Builder class with required properties
    public static class Builder implements IVersion, IBuild {
        Context properties = new Context();

        private Builder() {}

        /**

         * <p>
         * Must be followed by by additional optional properties or build() method
         */
        public Builder version(String version) {
            this.properties.properties.put("version", version);
            return this;
        }

        public Context build() {
            return new Context(this);
        }
    }

    // Required property interfaces
    public interface IVersion {
        Builder version(String version);
    }

    /** Build interface with optional properties */
    public interface IBuild {
        Context build();
    }
}
