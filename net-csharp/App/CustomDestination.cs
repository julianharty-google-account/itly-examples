using System;
using Iteratively;

namespace App
{
    class CustomDestination: IDestination {
        public void Init() {

        }
        public void Alias(string userId, string previousId) {

        }
        public void Identify(string userId, Properties properties) {

        }
        public void Group(string userId, string groupId, Properties properties) {

        }
        public void Track(string userId, string eventName, Properties properties) {
            Console.WriteLine($"Tracking {userId} {eventName} {properties}");

        }
        public void Dispose() {

        }
    }
}
