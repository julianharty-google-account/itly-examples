package ly.iterative.examples.java;

import io.itly.*;

import org.json.JSONObject;

public class CustomAdapter implements IItlyDestination {
    public String destinationName() {
        return "CustomAdapter";
    }

    public void init() {
        System.out.println("CustomAdapter.init()");
    }

    public void alias(String userId, String previousId) {
        System.out.println(String.format("CustomAdapter.alias(userId: %s, previousId: %s)", userId, previousId));
    }

    public void identify(String userId, ItlyPropertyMap properties) {
        System.out.println(String.format("CustomAdapter.identify(userId: %s, properties: %s)", userId, (properties == null) ? "null": properties.toJson().toString()));
    }

    public void group(String userId, String groupId, ItlyPropertyMap properties) {
        System.out.println(String.format("CustomAdapter.group(groupId: %s, properties: %s)", groupId, (properties == null) ? "null": properties.toJson().toString()));
    }
    public void reset() {
        System.out.println("CustomAdapter.reset()");
    }

    public void track(String userId, String eventName, ItlyPropertyMap properties) {
        System.out.println(String.format("CustomAdapter.track(eventName: %s, userId: %s, properties: %s)", eventName, userId, (properties == null) ? "null": properties.toJson().toString()));
    }
}
