package io.itly;

import java.util.HashMap;

public class OptionsItemSelected extends ItlyPropertyMap {
    private OptionsItemSelected() {}

    private OptionsItemSelected(Builder builder) {
        this.properties.putAll(builder.properties.properties);
    }

    private OptionsItemSelected(ItlyPropertyMap clone) {
        this.properties.putAll((HashMap<String, Object>) clone.properties.clone());
    }

    public OptionsItemSelected clone() {
        return new OptionsItemSelected(this);
    }

    public static IId builder() { return new Builder(); }

    // Inner Builder class with required properties
    public static class Builder implements IId, ITitle, IBuild {
        OptionsItemSelected properties = new OptionsItemSelected();

        private Builder() {}

        /**
         * The ID of the item.
         * <p>
         * Must be followed by {@link ITitle#title(String)
         */
        public ITitle id(Integer id) {
            this.properties.properties.put("id", id);
            return this;
        }

        /**
         * The title of the item.
         * <p>
         * Must be followed by by additional optional properties or build() method
         */
        public Builder title(String title) {
            this.properties.properties.put("title", title);
            return this;
        }

        public OptionsItemSelected build() {
            return new OptionsItemSelected(this);
        }
    }

    // Required property interfaces
    public interface IId {
        ITitle id(Integer id);
    }

    public interface ITitle {
        Builder title(String title);
    }

    /** Build interface with optional properties */
    public interface IBuild {
        OptionsItemSelected build();
    }
}
