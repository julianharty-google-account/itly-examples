import { PluginBase } from '@itly/sdk';

export default class extends PluginBase {
  id() {
    console.log(`CustomDestination: id()`);
    return 'CustomDestination';
  }

  load(options) {
    console.log(`CustomDestination: load()`, options);
  }

  track(_, event) {
    console.log(`CustomDestination: track()`, event);
  }
}
