// 
// JsonSerializerFactory.cs
// 
// This file is auto-generated by Iteratively.
// To update run 'itly pull dotnet'
// 
// Questions? We're here to help:
// https://www.iterative.ly/docs/interacting-with-the-sdk/#c-sharp
// 

using System;
using Manatee.Json.Serialization;

namespace Iteratively
{
    internal static class JsonSerializerFactory
    {
        private static readonly JsonSerializerOptions options = new JsonSerializerOptions()
        {
            EncodeDefaultValues = true
        };

        public static JsonSerializer Create()
        {
            return new JsonSerializer()
            {
                Options = options
            };
        }
    }
}