//
//  AppDelegate.swift
//  ItlySwiftCarthageExample
//
//  Created by Ondrej Hrebicek on 11/9/20.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Load Itly with no custom plugins or logger
        // Itly.instance.load()

        // Or, load Itly with a custom plugin and logger
        Itly.instance.load(
            options: Options(
                environment: Environment.development,
                plugins: [CustomPlugin()],
                logger: CustomLogger()
            )
        )

        // Track the Application Launched event
        Itly.instance.applicationLaunched()

        // Track the View Loaded event with the name property set to FirstView
        Itly.instance.viewLoaded(name: ViewLoaded.Name.firstView)
        // Track the View Loaded event with the name property set to SecondView (using the generic track() method)
        Itly.instance.track(ViewLoaded(name: ViewLoaded.Name.secondView))

        // Flush all pending events (optional, all events get flushed eventually by individual plugins)
        Itly.instance.flush()

        // Override point for customization after application launch.
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

