import { PluginBase, Options, Event } from '@itly/sdk';

export default class extends PluginBase {
  id() {
    console.log(`CustomDestination: id()`);
    return 'CustomDestination';
  }

  load(options: Options) {
    console.log(`CustomDestination: load()`, options);
  }

  track(_: string, event: Event) {
    console.log(`CustomDestination: track()`, event);
  }
}
