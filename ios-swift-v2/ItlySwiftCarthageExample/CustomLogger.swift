//
//  CustomLogger.swift
//  ItlySwiftCarthageExample
//
//  Created by Ondrej Hrebicek on 11/18/20.
//

import Foundation

class CustomLogger: Logger {
    public func debug(_ message: String) {
        print("CustomLogger [debug]: \(message)")
    }

    public func info(_ message: String) {
        print("CustomLogger [info]: \(message)")
    }

    public func warn(_ message: String) {
        print("CustomLogger [warn]: \(message)")
    }

    public func error(_ message: String) {
        print("CustomLogger [error]: \(message)")
    }
}

