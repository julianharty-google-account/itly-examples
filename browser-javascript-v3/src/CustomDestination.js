import { Plugin } from '@itly/sdk';

export default class extends Plugin {
  constructor() {
    super('custom-destination');
  }

  load(options) {
    console.log(`Custom Destination: load()`, options);
  }

  track(_, event, options) {
    console.log(`Custom Destination: track()`, event, options);
    console.log(`Custom Destination: awesome: ${options?.awesome}`);
  }
}
