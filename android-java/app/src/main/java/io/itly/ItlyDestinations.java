package io.itly;

public class ItlyDestinations {
    ItlyAmplitudeOptions amplitude;
    ItlyCustomOptions custom;
    ItlyMixpanelOptions mixpanel;
    ItlySegmentOptions segment;

    private ItlyDestinations(Builder builder) {
       this.amplitude = builder.amplitude;
       this.custom = builder.custom;
       this.mixpanel = builder.mixpanel;
       this.segment = builder.segment;
    }

    public static IAmplitude builder() {
        return new Builder();
    }

    public static class Builder implements IAmplitude, ICustom, IMixpanel, ISegment, IBuild {
        ItlyAmplitudeOptions amplitude;
        ItlyCustomOptions custom;
        ItlyMixpanelOptions mixpanel;
        ItlySegmentOptions segment;

        private Builder() {}

        public ICustom amplitude(ItlyAmplitudeOptions amplitude) {
            this.amplitude = amplitude;
            return this;
        }

        public IMixpanel custom(ItlyCustomOptions custom) {
            this.custom = custom;
            return this;
        }

        public ISegment mixpanel(ItlyMixpanelOptions mixpanel) {
            this.mixpanel = mixpanel;
            return this;
        }

        public IBuild segment(ItlySegmentOptions segment) {
            this.segment = segment;
            return this;
        }
        public ItlyDestinations build() {
            return new ItlyDestinations(this);
        }
    }

    public interface IAmplitude {
        ICustom amplitude(ItlyAmplitudeOptions amplitude);
    }
    public interface ICustom {
        IMixpanel custom(ItlyCustomOptions custom);
    }
    public interface IMixpanel {
        ISegment mixpanel(ItlyMixpanelOptions mixpanel);
    }
    public interface ISegment {
        IBuild segment(ItlySegmentOptions segment);
    }

    public interface IBuild {
        ItlyDestinations build();
    }
}
