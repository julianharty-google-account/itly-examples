//
//  CustomLogger.swift
//  IterativelySwift
//
//  Created by Ondrej Hrebicek on 11/5/20.
//  Copyright © 2020 Iteratively Inc. All rights reserved.
//

import Foundation

public class CustomLogger: ItlyLogger {
    public func debug(_ message: String) {
        print("[ITLY] Debug: \(message)")
    }

    public func info(_ message: String) {
        print("[ITLY] Info: \(message)")
    }

    public func warn(_ message: String) {
        print("[ITLY] Warning: \(message)")
    }

    public func error(_ message: String) {
        print("[ITLY] Error: \(message)")
    }
}
