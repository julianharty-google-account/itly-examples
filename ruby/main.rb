$:.unshift File.expand_path('../app', __FILE__)

ENV['BUNDLE_GEMFILE'] ||= File.expand_path('./Gemfile', __dir__)

require 'rubygems'
require 'bundler/setup'

# Run app
require 'app'
