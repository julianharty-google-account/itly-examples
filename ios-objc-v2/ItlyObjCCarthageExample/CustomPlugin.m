//
//  CustomPlugin.m
//  ItlyObjCCarthageExample
//
//  Created by Ondrej Hrebicek on 11/18/20.
//

#import "CustomPlugin.h"

@implementation CustomPlugin: ITLPlugin
- (instancetype)init {
    if (self = [super initWithId:@"custom-plugin"]) {
    }
    return self;
}

- (void)load:(ITLItlyOptions *)options {
    [options.logger debug:@"custom-plugin load"];
}

@end
