package io.itly;

public abstract class ItlyLogger {
    public abstract void debug(String message);
    public abstract void info(String message);
    public abstract void warn(String message);
    public abstract void error(String message);
}
