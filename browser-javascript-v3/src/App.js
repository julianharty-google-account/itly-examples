import React from 'react';
import logo from './logo.svg';
import './App.css';
import itly from './itly';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <p>
        <button onClick={() => {
          itly.buttonClicked(
            /* event's properties */
            { label: 'Send Event' },
            /* track options for custom-destination plugin */
            { 'custom-destination': { awesome: true } },
          )
        }}>
          Send Event
        </button>
        &nbsp;
        <button onClick={() => itly.alertButtonClicked(
          /* event's properties */
          { alertKind: 'red' },
        )}>
          Alert
        </button>
        </p>
      </header>
    </div>
  );
}

export default App;
