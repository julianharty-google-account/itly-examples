package io.itly;

import java.util.Iterator;
import java.util.Map;
import org.json.*;

import com.amplitude.api.Amplitude;

public class ItlyAmplitudeAdapter implements IItlyDestination {
    private ItlyOptions options = null;

    ItlyAmplitudeAdapter(ItlyOptions options, String apiToken) {
        this.options = options;
        Amplitude.getInstance().initialize(options.destinations.amplitude.getAndroidContext(), apiToken);
    }

    public void init() {
        // N/A for Amplitude
    }

    public void alias(String userId, String previousId) {
        // N/A for Amplitude
    }

    public void identify(String userId, ItlyPropertyMap identifyProperties) {
        if (userId != null) {
            Amplitude.getInstance().setUserId(userId);
        }

        if (identifyProperties == null) {
            return;
        }

        com.amplitude.api.Identify identify = new com.amplitude.api.Identify();
        Iterator<Map.Entry<String, Object>> contextKeys = identifyProperties.getEntrySet().iterator();
        while (contextKeys.hasNext()) {
            Map.Entry<String, Object> property = contextKeys.next();
            Object obj = property.getValue();
            String key = property.getKey();

            if (obj instanceof Integer) {
                identify.set(key, (int) obj);
            }
            else if (obj instanceof Long) {
                identify.set(key, (long) obj);
            }
            else if (obj instanceof Float) {
                identify.set(key, (float) obj);
            }
            else if (obj instanceof Double) {
                identify.set(key, (double) obj);
            }
            else if (obj instanceof String) {
                identify.set(key, (String) obj);
            }
            else if (obj instanceof Boolean) {
                identify.set(key, (boolean) obj);
            }
            else if (obj instanceof int[]) {
                identify.set(key, (int[]) obj);
            }
            else if (obj instanceof long[]) {
                identify.set(key, (long[]) obj);
            }
            else if (obj instanceof float[]) {
                identify.set(key, (float[]) obj);
            }
            else if (obj instanceof double[]) {
                identify.set(key, (double[]) obj);
            }
            else if (obj instanceof String[]) {
                identify.set(key, (String[]) obj);
            }
            else if (obj instanceof Boolean[]) {
                identify.set(key, (boolean[]) obj);
            }
            else {
                System.err.println("Invalid type encountered for object: " + obj);
            }
        }

        Amplitude.getInstance().identify(identify);
    }

    public void group(String groupId, ItlyPropertyMap groupProperties) {
        // N/A for Amplitude
    }

    public void track(String userId, String eventName, ItlyPropertyMap eventProperties) {
        JSONObject propertiesJson = eventProperties.toJson();
        Amplitude.getInstance().logEvent(eventName, propertiesJson);
    }

    public void reset() {
        Amplitude.getInstance().setUserId(null);
    }
}
