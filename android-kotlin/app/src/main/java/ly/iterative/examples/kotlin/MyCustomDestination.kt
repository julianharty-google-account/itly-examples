package ly.iterative.examples.kotlin

import io.itly.*

class MyCustomDestination : IDestination {
    override fun init() {
        println("MyCustomDestination.init()")
    }

    override fun alias(userId: String, previousId: String?) {
        println(
            String.format(
                "MyCustomDestination.alias(userId: %s, previousId: %s)",
                userId,
                previousId
            )
        )
    }

    override fun identify(userId: String?, properties: ConvertibleProperties?) {
        println(
            String.format(
                "MyCustomDestination.identify(userId: %s, properties: %s)",
                userId,
                properties?.asOrgJson?.toString() ?: "null"
            )
        )
    }

    override fun group(groupId: String, properties: ConvertibleProperties?) {
        println(
            String.format(
                "MyCustomDestination.group(groupId: %s, properties: %s)",
                groupId,
                properties?.asOrgJson?.toString() ?: "null"
            )
        )
    }

    override fun reset() {
        println("MyCustomDestination.reset()")
    }

    override fun track(userId: String?, eventName: String, properties: ConvertibleProperties?) {
        println(
            String.format(
                "MyCustomDestination.track(eventName: %s, userId: %s, properties: %s)",
                eventName,
                userId,
                properties?.asOrgJson?.toString() ?: "null"
            )
        )
    }
}
