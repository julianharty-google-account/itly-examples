//
//  ValidationOptions.kt
//  This file is auto-generated by Iteratively. Run `itly pull kotlin` to update.
//

package io.itly

class ValidationOptions(
    val disabled: Boolean = false,
    val failOnError: Boolean
)