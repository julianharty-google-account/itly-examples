package io.itly;

public class ItlyDestinations {
    ItlyCustomOptions custom;
    ItlySegmentOptions segment;

    private ItlyDestinations(Builder builder) {
       this.custom = builder.custom;
       this.segment = builder.segment;
    }

    public static ICustom builder() {
        return new Builder();
    }

    public static class Builder implements ICustom, ISegment, IBuild {
        ItlyCustomOptions custom;
        ItlySegmentOptions segment;

        private Builder() {}

        public ISegment custom(ItlyCustomOptions custom) {
            this.custom = custom;
            return this;
        }

        public IBuild segment(ItlySegmentOptions segment) {
            this.segment = segment;
            return this;
        }
        public ItlyDestinations build() {
            return new ItlyDestinations(this);
        }
    }

    public interface ICustom {
        ISegment custom(ItlyCustomOptions custom);
    }
    public interface ISegment {
        IBuild segment(ItlySegmentOptions segment);
    }

    public interface IBuild {
        ItlyDestinations build();
    }
}
