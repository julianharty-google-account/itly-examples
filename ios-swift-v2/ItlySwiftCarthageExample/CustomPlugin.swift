//
//  CustomPlugin.swift
//  ItlySwiftCarthageExample
//
//  Created by Ondrej Hrebicek on 11/18/20.
//

import Foundation

class CustomPlugin: Plugin {
    private weak var logger: Logger?
    
    override init() {
        super.init(id: "custom-plugin")
    }
    
    public override func load(_ options: Options) {
        super.load(options)
        
        logger = options.logger
        logger?.debug("\(self.id) load()")
    }
    
    override func validate(_ event: Event) -> ValidationResponse {
        logger?.debug("\(self.id) validate(event=\(event.name))")
        return ValidationResponse(valid: true)
    }
    
    override func alias(_ userId: String, previousId: String?) {
        logger?.debug("\(self.id) alias(userId=\(userId) previousId=\(str(previousId)))")
    }
    override func postAlias(_ userId: String, previousId: String?) {
        logger?.debug("\(self.id) postAlias(userId=\(userId) previousId=\(str(previousId)))")
    }
    
    override func identify(_ userId: String?, properties: Properties?) {
        logger?.debug("\(self.id) identify(userId=\(str(userId)) properties=\(str(properties?.properties)))")
    }
    override func postIdentify(_ userId: String?, properties: Properties?, validationResults: [ValidationResponse]) {
        logger?.debug("\(self.id) postIdentify(userId=\(str(userId)) properties=\(str(properties?.properties)))")
    }

    override func group(_ userId: String?, groupId: String, properties: Properties?) {
        logger?.debug("\(self.id) group(groupId=\(groupId) properties=\(str(properties)))")
    }
    override func postGroup(_ userId: String?, groupId: String, properties: Properties?, validationResults: [ValidationResponse]) {
        logger?.debug("\(self.id) postGroup(groupId=\(groupId) properties=\(str(properties?.properties)))")
    }
    
    override func track(_ userId: String?, event: Event) {
        logger?.debug("\(self.id) track(event=\(event.name) properties=\(str(event.properties)))")
    }
    override func postTrack(_ userId: String?, event: Event, validationResults: [ValidationResponse]) {
        logger?.debug("\(self.id) postTrack(event=\(event.name) properties=\(str(event.properties)))")
    }
    
    override func reset() {
        logger?.debug("\(self.id) reset()")
    }
    
    override func flush() {
        logger?.debug("\(self.id) flush()")
    }
    
    override func shutdown() {
        logger?.debug("\(self.id) shutdown()")
    }
    
    private func str(_ what: Any?) -> String {
        return String(describing: what);
    }
}

