package io.itly;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

public class ItlyMixpanelAdapter implements IItlyDestination {
    private MixpanelAPI mixpanel;

    ItlyMixpanelAdapter(ItlyOptions options, String apiToken) {
        this.mixpanel = MixpanelAPI.getInstance(options.destinations.mixpanel.getAndroidContext(), apiToken);
    }

    public void init() {
        // N/A for Mixpanel
    }

    public void alias(String userId, String previousId) {
        /* The recommended usage pattern is to call both alias and identify when the user signs up (as shown in the example above),
         * and only identify (with the aliased user ID) on future log ins. This will keep your signup funnels working correctly.
         */
        this.mixpanel.alias(userId, previousId);
    }

    public void identify(String userId, ItlyPropertyMap identifyProperties) {
        this.mixpanel.identify(userId);
        this.mixpanel.getPeople().identify(userId);;
        if (identifyProperties != null) {
            this.mixpanel.getPeople().set(identifyProperties.toJson());
        }
    }

    public void group(String groupId, ItlyPropertyMap groupProperties) {
        // N/A for Mixpanel
    }

    public void track(String userId, String eventName, ItlyPropertyMap properties) {
        this.mixpanel.track(eventName, properties.toJson());
    }

    public void reset() {
        mixpanel.reset();
    }
}
