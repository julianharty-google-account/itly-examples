package io.itly;

public class ItlyAmplitudeOptions implements IItlyAdapterOptions {
    private android.content.Context androidContext;

    public ItlyAmplitudeOptions(android.content.Context androidContext) {
        this.androidContext = androidContext;
    }

    public android.content.Context getAndroidContext() {
        return this.androidContext;
    }
}
