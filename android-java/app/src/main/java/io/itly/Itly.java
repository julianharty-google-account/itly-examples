package io.itly;

import java.util.*;
import java.lang.*;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONException;
import org.json.JSONObject;

public class Itly {
    private ItlyOptions options;
    private ArrayList<IItlyDestination> adapters = new ArrayList<IItlyDestination>();
    private static volatile Itly singleton = null;

    private static final Map<String, JSONObject> schemaValidators = new HashMap<String, JSONObject>();

    private Itly() {
    }

    /**
     * Initializes Itly with configured destinations
     * @param options Configuration for Itly instance.
     */
    public void init(ItlyOptions options) {
        if (this.options != null) {
            throw new Error("Itly is already initialized.");
        }

        this.options = options;

        this.adapters.add(new ItlyAmplitudeAdapter(options, this.options.environment == ItlyOptions.Environment.PRODUCTION ? "6c3a9cd0a22daafe3278346608a816cf" : "6c3a9cd0a22daafe3278346608a816cf"));
        this.adapters.add(options.destinations.custom.getAdapter());
        this.adapters.add(new ItlyMixpanelAdapter(options, this.options.environment == ItlyOptions.Environment.PRODUCTION ? "67a8ece0a81e35124d7c23c06b04c52f" : "67a8ece0a81e35124d7c23c06b04c52f"));
        this.adapters.add(new ItlySegmentAdapter(options, this.options.environment == ItlyOptions.Environment.PRODUCTION ? "CvU1TXIk6zSKuN7Vyx3FMT7cqzyvY5th" : "CvU1TXIk6zSKuN7Vyx3FMT7cqzyvY5th"));

        for (IItlyDestination adapter : this.adapters) {
            adapter.init();
        }
    }

    /**
     * Returns singleton instance of Itly
     */
    public static Itly getInstance() {
        if (singleton == null) {
            createSingleton();
        }
        return singleton;
    }

    private synchronized static void createSingleton() {
        if (singleton == null) {
            singleton = new Itly();
            try {
                schemaValidators.put("Context", new JSONObject("{\"$id\":\"https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/context\",\"$schema\":\"http://json-schema.org/draft-07/schema#\",\"title\":\"Context\",\"description\":\"\",\"type\":\"object\",\"properties\":{\"version\":{\"description\":\"\",\"type\":\"string\"}},\"additionalProperties\":false,\"required\":[\"version\"]}"));
                schemaValidators.put("Identify", new JSONObject("{\"$id\":\"https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/identify\",\"$schema\":\"http://json-schema.org/draft-07/schema#\",\"title\":\"Identify\",\"description\":\"\",\"type\":\"object\",\"properties\":{\"firstName\":{\"description\":\"The user's first name.\",\"type\":\"string\"},\"lastName\":{\"description\":\"The user's last name.\",\"type\":\"string\"}},\"additionalProperties\":false,\"required\":[\"firstName\",\"lastName\"]}"));
                schemaValidators.put("ActivityCreated", new JSONObject("{\"$id\":\"https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/event/3b881d1d-cf1d-42aa-b8ac-b6cdebe00cf0\",\"$schema\":\"http://json-schema.org/draft-07/schema#\",\"title\":\"Activity Created\",\"description\":\"Called when the application launches.\",\"type\":\"object\",\"properties\":{\"title\":{\"description\":\"The name of the activity.\",\"enum\":[\"MainActivity\"]}},\"additionalProperties\":false,\"required\":[\"title\"]}"));
                schemaValidators.put("MailButtonClicked", new JSONObject("{\"$id\":\"https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/event/aaa578f6-387c-4348-a759-201fe79d7460\",\"$schema\":\"http://json-schema.org/draft-07/schema#\",\"title\":\"Mail Button Clicked\",\"description\":\"Called when the user clicks the envelope button.\",\"type\":\"object\",\"properties\":{},\"additionalProperties\":false,\"required\":[]}"));
                schemaValidators.put("OptionsItemSelected", new JSONObject("{\"$id\":\"https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/event/292ef784-67cf-43f9-aee6-e1231f59f482\",\"$schema\":\"http://json-schema.org/draft-07/schema#\",\"title\":\"Options Item Selected\",\"description\":\"Called when the user clicks on an item in the options menu.\",\"type\":\"object\",\"properties\":{\"id\":{\"description\":\"The ID of the item.\",\"type\":\"integer\"},\"title\":{\"description\":\"The title of the item.\",\"type\":\"string\"}},\"additionalProperties\":false,\"required\":[\"id\",\"title\"]}"));
                schemaValidators.put("OptionsOpened", new JSONObject("{\"$id\":\"https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/event/9e444700-b75a-4996-9457-ca928ca093c3\",\"$schema\":\"http://json-schema.org/draft-07/schema#\",\"title\":\"Options Opened\",\"description\":\"Called when the user opens the options menu.\",\"type\":\"object\",\"properties\":{},\"additionalProperties\":false,\"required\":[]}"));
                schemaValidators.put("SnackbarDisplayed", new JSONObject("{\"$id\":\"https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/event/b3c8371c-05f6-4dce-9625-24e8e7e6a3c3\",\"$schema\":\"http://json-schema.org/draft-07/schema#\",\"title\":\"Snackbar Displayed\",\"description\":\"Called when the application displays a snackbar.\",\"type\":\"object\",\"properties\":{\"length\":{\"description\":\"The size of the snackbar.\",\"type\":\"integer\"},\"text\":{\"description\":\"The contents of the snackbar.\",\"type\":\"string\"}},\"additionalProperties\":false,\"required\":[\"length\",\"text\"]}"));
            } catch (JSONException e) {}
        }
    }

    /**
     * Alias a user ID.
     * @param userId The user's new ID.
     */
    public void alias(String userId) {
        alias(userId, null);
    }

    /**
     * Alias a user ID to another user ID.
     * @param userId The user's new ID.
     * @param previousId The user's previous ID.
     */
    public void alias(String userId, String previousId) {
        if (userId == null) {
            throw new IllegalArgumentException("userId cannot be null");
        }

        if (!this.isInitializedAndEnabled()) {
            return;
        }

        for (IItlyDestination adapter : this.adapters) {
            adapter.alias(userId, previousId);
        }
    }

    /**
     * Set or update a user's properties.
     * @param properties Required and optional user properties.
     */
    public void identify(Identify properties) {
        identify(null, properties);
    }

    /**
     * Set or update a user's properties.
     * @param userId The user's ID.
     * @param properties Required and optional user properties.
     */
    public void identify(String userId, Identify properties) {
        if (properties == null) {
            throw new IllegalArgumentException("properties cannot be null");
        }

        if (!this.isInitializedAndEnabled()) {
            return;
        }

        validateSchema(properties, "Identify");

        for (IItlyDestination adapter : this.adapters) {
            adapter.identify(userId, properties);
        }
    }

    /**
    * Set or update a group's properties.
    * @param groupId The group's ID.
    */
   public void group(String groupId) {
       if (!this.isInitializedAndEnabled()) {
           return;
       }

       for (IItlyDestination adapter : this.adapters) {
           adapter.group(groupId, null);
       }
   }
    /**
     * Called when the application launches.
     * 
     * Owner: Ondrej Hrebicek (no SSO)
     * @param properties The event's properties (e.g. title)
     */
    public void trackActivityCreated(ActivityCreated properties) {
        if (properties == null) {
            throw new IllegalArgumentException("properties cannot be null");
        }

        this.trackEvent(
            "Activity Created",
            properties,
            "https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/event/3b881d1d-cf1d-42aa-b8ac-b6cdebe00cf0",
            "ActivityCreated"
        );
    }
    /**
     * Called when the user clicks the envelope button.
     * 
     * Owner: Ondrej Hrebicek (no SSO)
     */
    public void trackMailButtonClicked() {
        this.trackEvent(
            "Mail Button Clicked",
            null,
            "https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/event/aaa578f6-387c-4348-a759-201fe79d7460",
            "MailButtonClicked"
        );
    }
    /**
     * Called when the user clicks on an item in the options menu.
     * 
     * Owner: Ondrej Hrebicek (no SSO)
     * @param properties The event's properties (e.g. id)
     */
    public void trackOptionsItemSelected(OptionsItemSelected properties) {
        if (properties == null) {
            throw new IllegalArgumentException("properties cannot be null");
        }

        this.trackEvent(
            "Options Item Selected",
            properties,
            "https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/event/292ef784-67cf-43f9-aee6-e1231f59f482",
            "OptionsItemSelected"
        );
    }
    /**
     * Called when the user opens the options menu.
     * 
     * Owner: Ondrej Hrebicek (no SSO)
     */
    public void trackOptionsOpened() {
        this.trackEvent(
            "Options Opened",
            null,
            "https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/event/9e444700-b75a-4996-9457-ca928ca093c3",
            "OptionsOpened"
        );
    }
    /**
     * Called when the application displays a snackbar.
     * 
     * Owner: Ondrej Hrebicek (no SSO)
     * @param properties The event's properties (e.g. length)
     */
    public void trackSnackbarDisplayed(SnackbarDisplayed properties) {
        if (properties == null) {
            throw new IllegalArgumentException("properties cannot be null");
        }

        this.trackEvent(
            "Snackbar Displayed",
            properties,
            "https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/event/b3c8371c-05f6-4dce-9625-24e8e7e6a3c3",
            "SnackbarDisplayed"
        );
    }

    /**
     * Called when user is logged out.
     */
    public void reset() {
        if (!this.isInitializedAndEnabled()) {
            return;
        }

        for (IItlyDestination adapter : this.adapters) {
            adapter.reset();
        };
    }

    private void trackEvent(String eventName, ItlyPropertyMap properties, String itlySchema, String validatorKey) {
        if (!this.isInitializedAndEnabled()) {
            return;
        }

        validateSchema(properties, validatorKey);
        validateSchema(this.options.context, "Context");

        // If duplicate property name exists, last one wins. In order of priority: properties, then context, then itly properties
        ItlyPropertyMap combinedPropertyMap = new ItlyProperties(itlySchema).concat(new ItlyPropertyMap[] {this.options.context, properties});

        for (IItlyDestination adapter : this.adapters) {
            try {
                adapter.track(null, eventName, combinedPropertyMap);
            } catch (Exception e) {
                options.logger.error(String.format("Exception thrown in '%s.trackEvent' for event '%s' with message '%s'", adapter.getClass().getName(), eventName, e.getMessage()));
            }
        }
    }

    private boolean isInitializedAndEnabled() throws Error {
        if (this.options == null) {
            throw new Error("Itly is not initialized.");
        }

        return !this.options.disabled;
    }

    private void validateSchema(ItlyPropertyMap properties, String validatorKey) {
        if (properties == null) {
            // All events with properties have had this checked.
            return;
        }

        try {
            // Code generator generates a SchemaValidator for every event and, if applicable, Context, Group, and Identify.
            SchemaLoader loader = SchemaLoader.builder()
                .schemaJson(Itly.schemaValidators.get(validatorKey))
                .draftV7Support()
                .build();
            Schema schema = loader.load().build();
            schema.validate(properties.toJson());
        }
        catch (ValidationException e) {
            StringBuilder errorsBuilder = new StringBuilder();
            for (String error : e.getAllMessages()) {
                errorsBuilder.append(error);
            }

            this.handleValidationError(String.format("Itly validation error: %s", errorsBuilder));
        }
    }

    private void handleValidationError(String message) {
        if (this.options.logger != null) {
            this.options.logger.error(message);
        } else {
            System.err.println(message);
        }

        if (this.options.environment != ItlyOptions.Environment.PRODUCTION) {
            throw new IllegalArgumentException(message);
        }
    }
}
