package io.itly;

import org.json.*;
import java.util.*;

public abstract class ItlyPropertyMap {
    protected final HashMap<String, Object> properties = new HashMap<String, Object>();
    protected JSONObject propertiesJson = null;

    public int getSize() {
        return properties.size();
    }

    public abstract ItlyPropertyMap clone();

    /*
     * Returns Entry set of this properties.
     */
    public Set<Map.Entry<String, Object>> getEntrySet() {
        return properties.entrySet();
    }

    /*
     * Returns new concatenated HashMap instance.
     */
    public ItlyPropertyMap concat(ItlyPropertyMap[] concatMaps) {
        ItlyPropertyMap newObj = this.clone();

        for (ItlyPropertyMap map : concatMaps) {
            if (map == null) {
                continue;
            }
            Iterator<Map.Entry<String, Object>> concatEntries = map.getEntrySet().iterator();
            while (concatEntries.hasNext()) {
                Map.Entry<String, Object> concatEntry = concatEntries.next();
                // If key already exists, HashMap.put(value) replaces value with new value
                newObj.properties.put(concatEntry.getKey(), concatEntry.getValue());
            }
        }

        return newObj;
    }

    /*
     * Returns JSON object of this properties.
     */
    public JSONObject toJson() {
        if (propertiesJson == null) {
            propertiesJson = new JSONObject();
            Iterator<Map.Entry<String, Object>> eventPropertyEntries = properties.entrySet().iterator();
            while (eventPropertyEntries.hasNext()) {
                try {
                    Map.Entry<String, Object> propertyEntry = eventPropertyEntries.next();
                    Object value = propertyEntry.getValue();
                    value = (value == null) ? JSONObject.NULL : value;
                    propertiesJson.put(propertyEntry.getKey(), (value.getClass().isArray() ? new JSONArray(value) : value));
                } catch (JSONException exception) {
                    System.err.println("Error converting event properties for Amplitude to JSONObject: " + exception.getMessage());
                }
            }
        }
        return propertiesJson;
    }
}
