/* tslint:disable */
/* eslint-disable */
import itly, {
  Environment,
  Event as BaseEvent,
  Properties as BaseProperties,
  Plugin as BasePlugin,
  PluginBase as BasePluginBase,
  ValidationOptions as BaseValidationOptions,
  ValidationResponse as BaseValidationResponse,
} from '@itly/sdk';
import SchemaValidatorPlugin from '@itly/plugin-schema-validator';
import IterativelyPlugin, { IterativelyOptions as BaseIterativelyOptions } from '@itly/plugin-iteratively';
import AmplitudePlugin, { AmplitudeOptions } from '@itly/plugin-amplitude';
import MixpanelPlugin, { MixpanelOptions } from '@itly/plugin-mixpanel';
import SegmentPlugin, { SegmentOptions } from '@itly/plugin-segment';

type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
type WithOptional<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;

export type Event = BaseEvent;
export type Properties = BaseProperties;
export interface Plugin extends BasePlugin {};
export class PluginBase extends BasePluginBase {};
export type ValidationOptions = BaseValidationOptions;
export type ValidationResponse = BaseValidationResponse;
export type IterativelyOptions = WithOptional<BaseIterativelyOptions, 'url' | 'environment'>;

export interface ContextProperties {
  version: string;
}
export interface IdentifyProperties {
  /**
   * The user's last name.
   */
  lastName: string;
  /**
   * The user's first name.
   */
  firstName: string;
}
export interface SendEventButtonClickedProperties {}

export class SendEventButtonClicked implements Event {
  name = 'Send Event Button Clicked';
  id = '5c2c7b63-6161-4f70-b6ef-0fec926fc2c4';
  version = '2.0.0';
}

// prettier-ignore
export interface Options {
  /**
   * The current environment (development or production). Default is development.
   */
  environment?: Environment;
  /**
   * Whether calls to the Itly SDK should be no-ops. Default is false.
   */
  disabled?: boolean;
  /**
   * Analytics provider-specific configuration. Default is null.
   */
  plugins?: Plugin[];
  /**
   * Configure validation handling
   */
  validation?: ValidationOptions;
  /**
   * Analytics provider-specific configuration. Default is null.
   */
  destinations?: {
    iteratively?: IterativelyOptions;
    amplitude?: AmplitudeOptions;
    mixpanel?: MixpanelOptions;
    segment?: SegmentOptions;
    all?: {
      disabled?: boolean;
    };
  };
  /**
   * Additional context properties to add to all events.
   */
  context: ContextProperties;
}



// prettier-ignore
class Itly {
  load(options: Options) {
    const {
      destinations = {},
      plugins = [],
      ...baseOptions
    } = options;

    const destinationPlugins = destinations.all && destinations.all.disabled
      ? []
      : [
        new IterativelyPlugin(options.environment === 'production'
          ? 'qMLH5al4bFPem0lsWPnSnH1PIHR4vqr-'
          : 'UOx-djeEFdp9cbxUrPfOGxAhcg6Rog5m',
          {
            url: 'https://api.iterative.ly/t/version/9616e203-e3cc-44fa-9338-84f61eb78f4d',
            environment: options.environment || 'development',
            ...destinations.iteratively,
          },
        ),
        new AmplitudePlugin(options.environment === 'production'
          ? '6c3a9cd0a22daafe3278346608a816cf'
          : '6c3a9cd0a22daafe3278346608a816cf',
          destinations.amplitude,
        ),
        new MixpanelPlugin(options.environment === 'production'
          ? '67a8ece0a81e35124d7c23c06b04c52f'
          : '67a8ece0a81e35124d7c23c06b04c52f',
          destinations.mixpanel,
        ),
        new SegmentPlugin(options.environment === 'production'
          ? 'vTHA3Cg3K6BVBsRZasHj45glQj0Br2c0'
          : 'vTHA3Cg3K6BVBsRZasHj45glQj0Br2c0',
          destinations.segment,
        ),
      ];

    itly.load({
      ...baseOptions,
      plugins: [
        new SchemaValidatorPlugin({
          'context': {"$id":"https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/context","$schema":"http://json-schema.org/draft-07/schema#","title":"Context","description":"","type":"object","properties":{"version":{"description":"","type":"string"}},"additionalProperties":false,"required":["version"]},
          'identify': {"$id":"https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/identify","$schema":"http://json-schema.org/draft-07/schema#","title":"Identify","description":"","type":"object","properties":{"lastName":{"description":"The user's last name.","type":"string"},"firstName":{"description":"The user's first name.","type":"string"}},"additionalProperties":false,"required":["lastName","firstName"]},
          'Send Event Button Clicked': {"$id":"https://iterative.ly/company/ec03772c-96b1-455d-9b19-fe4c5d8618f9/event/Send%20Event%20Button%20Clicked/version/2.0.0","$schema":"http://json-schema.org/draft-07/schema#","title":"Send Event Button Clicked","description":"Called when the user clicks the Send Event button.","type":"object","properties":{},"additionalProperties":false,"required":[]},
        }),
        ...destinationPlugins,
        ...plugins,
      ]
    });
  }

  /**
  * Alias a user ID to another user ID.
  * @param userId The user's new ID.
  * @param previousId The user's previous ID.
  */
  alias(userId: string, previousId?: string) {
    itly.alias(userId, previousId);
  }

  /**
  * Set or update a user's properties.
  * @param userId The user's ID.
  * @param properties Required and optional user properties.
  */
  identify(userId: string, properties: IdentifyProperties) {
    itly.identify(userId, properties)
  }

  group(groupId: string) {
    itly.group(groupId)
  }

  page(category: string, name: string) {
    itly.page(category, name);
  }

  /**
   * Called when the user clicks the Send Event button.
   * 
   * Owner: Ondrej Hrebicek (no SSO)
   */
  sendEventButtonClicked() {
    itly.track(new SendEventButtonClicked());
  }

  track(event: Event) {
    itly.track(event);
  }

  reset() {
    itly.reset();
  }
}

export default new Itly();
