package io.itly;

import java.util.HashMap;

public class ActivityCreated extends ItlyPropertyMap {
    public enum Title {
        MAINACTIVITY("MainActivity");

        private String title;

        public String getTitle()
        {
            return this.title;
        }

        Title(String title)
        {
            this.title = title;
        }
    }

    private ActivityCreated() {}

    private ActivityCreated(Builder builder) {
        this.properties.putAll(builder.properties.properties);
    }

    private ActivityCreated(ItlyPropertyMap clone) {
        this.properties.putAll((HashMap<String, Object>) clone.properties.clone());
    }

    public ActivityCreated clone() {
        return new ActivityCreated(this);
    }

    public static ITitle builder() { return new Builder(); }

    // Inner Builder class with required properties
    public static class Builder implements ITitle, IBuild {
        ActivityCreated properties = new ActivityCreated();

        private Builder() {}

        /**
         * The name of the activity.
         * <p>
         * Must be followed by by additional optional properties or build() method
         */
        public Builder title(Title title) {
            this.properties.properties.put("title", title.getTitle());
            return this;
        }

        public ActivityCreated build() {
            return new ActivityCreated(this);
        }
    }

    // Required property interfaces
    public interface ITitle {
        Builder title(Title title);
    }

    /** Build interface with optional properties */
    public interface IBuild {
        ActivityCreated build();
    }
}
