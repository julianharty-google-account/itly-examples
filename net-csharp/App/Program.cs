﻿using System;
using Iteratively;

namespace App
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting app...");

            Itly.Load(new Options(
                new Context(version: "1.0"),
                new DestinationsOptions(
                    new CustomOptions(new CustomDestination())
                )
            ));
            Itly.ProcessStarted("some-user-id",
                availableProcessors: System.Environment.ProcessorCount
            );
            Itly.Track("some-user-id", new ProcessStarted(System.Environment.ProcessorCount));

            Console.WriteLine("Ending app...");
            Itly.Dispose();
        }
    }
}
