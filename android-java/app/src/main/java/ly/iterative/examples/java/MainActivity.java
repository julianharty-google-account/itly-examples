package ly.iterative.examples.java;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import io.itly.*;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Itly.getInstance().init(ItlyOptions.builder()
            .destinations(ItlyDestinations.builder()
                .amplitude(new ItlyAmplitudeOptions(getApplicationContext()))
                .custom(new ItlyCustomOptions(new MyCustomDestination()))
                .mixpanel(new ItlyMixpanelOptions(getApplicationContext()))
                .segment(new ItlySegmentOptions(getApplicationContext()))
                .build())
            .context(Context.builder()
                .version("1.0")
                .build())
            .build()
        );

        Itly.getInstance().trackActivityCreated(ActivityCreated.builder()
            .title(ActivityCreated.Title.MAINACTIVITY)
            .build()
        );

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Itly.getInstance().trackMailButtonClicked();
                Itly.getInstance().trackSnackbarDisplayed(SnackbarDisplayed.builder()
                    .length(Snackbar.LENGTH_LONG)
                    .text("Replace with your own action")
                    .build()
                );

                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Itly.getInstance().trackOptionsOpened();
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Itly.getInstance().trackOptionsItemSelected(OptionsItemSelected.builder()
            .id(item.getItemId())
            .title(item.toString())
            .build()
        );
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
