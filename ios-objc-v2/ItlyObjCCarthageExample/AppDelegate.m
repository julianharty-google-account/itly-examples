//
//  AppDelegate.m
//  ItlyObjCCarthageExample
//
//  Created by Ondrej Hrebicek on 11/10/20.
//

#import "AppDelegate.h"
#import "Itly/Itly.h"
#import "CustomLogger.h"
#import "CustomPlugin.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Load Itly with no custom plugins or logger
    // [Itly.instance load];

    // Or, load Itly with a custom plugin and logger
    [Itly.instance load:nil options:[ITLItlyOptions builderBlock:^(ITLItlyOptionsBuilder *b) {
        b.environment = ITLEnvironmentDevelopment;
        b.plugins = @[[CustomPlugin new]];
        b.logger = [CustomLogger new];
    }]];

    // Track the Application Launched event
    [Itly.instance applicationLaunched];
    
    // Track the View Loaded event with the name property set to FirstView
    [Itly.instance viewLoadedWithName:ViewLoadedNameFirstView];
    // Track the View Loaded event with the name property set to SecondView (using the generic track method)
    [Itly.instance track:[ViewLoaded name:ViewLoadedNameSecondView]];
    
    // Flush all pending events (optional, all events get flushed eventually by individual plugins)
    [Itly.instance flush];

    // Override point for customization after application launch.
    return YES;
}


#pragma mark - UISceneSession lifecycle


- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
}


- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
}


@end
