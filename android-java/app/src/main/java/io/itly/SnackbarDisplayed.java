package io.itly;

import java.util.HashMap;

public class SnackbarDisplayed extends ItlyPropertyMap {
    private SnackbarDisplayed() {}

    private SnackbarDisplayed(Builder builder) {
        this.properties.putAll(builder.properties.properties);
    }

    private SnackbarDisplayed(ItlyPropertyMap clone) {
        this.properties.putAll((HashMap<String, Object>) clone.properties.clone());
    }

    public SnackbarDisplayed clone() {
        return new SnackbarDisplayed(this);
    }

    public static ILength builder() { return new Builder(); }

    // Inner Builder class with required properties
    public static class Builder implements ILength, IText, IBuild {
        SnackbarDisplayed properties = new SnackbarDisplayed();

        private Builder() {}

        /**
         * The size of the snackbar.
         * <p>
         * Must be followed by {@link IText#text(String)
         */
        public IText length(Integer length) {
            this.properties.properties.put("length", length);
            return this;
        }

        /**
         * The contents of the snackbar.
         * <p>
         * Must be followed by by additional optional properties or build() method
         */
        public Builder text(String text) {
            this.properties.properties.put("text", text);
            return this;
        }

        public SnackbarDisplayed build() {
            return new SnackbarDisplayed(this);
        }
    }

    // Required property interfaces
    public interface ILength {
        IText length(Integer length);
    }

    public interface IText {
        Builder text(String text);
    }

    /** Build interface with optional properties */
    public interface IBuild {
        SnackbarDisplayed build();
    }
}
