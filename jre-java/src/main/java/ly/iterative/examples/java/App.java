package ly.iterative.examples.java;

import io.itly.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Itly.getInstance().init(ItlyOptions.builder()
            .destinations(ItlyDestinations.builder()
                .custom(new ItlyCustomOptions(new CustomAdapter()))
                .segment(new ItlySegmentOptions())
                .build())
            .context(Context.builder()
                .version("1.0")
                .build())
            .logger(new Logger())
            .disabled(false)
            .environment(ItlyOptions.Environment.DEVELOPMENT)
            .build());

        Itly.getInstance().trackProcessStarted("some-user-id", ProcessStarted.builder()
            .availableProcessors(Runtime.getRuntime().availableProcessors())
            .build()
        );
    }
}
