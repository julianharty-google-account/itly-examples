//
//  CustomLogger.h
//  ItlyObjCCarthageExample
//
//  Created by Ondrej Hrebicek on 11/12/20.
//

#import "Itly.h"

#ifndef CustomLogger_h
#define CustomLogger_h

@interface CustomLogger : NSObject <ITLLogger>

@end

#endif /* CustomLogger */
