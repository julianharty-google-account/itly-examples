//
//  Group.kt
//  This file is auto-generated by Iteratively. Run `itly pull kotlin` to update.
//

package ly.iterative.itly

/**
 * Group properties.
 */
class Group() : Event(
    name = "group",
    id = "group",
    version = "46.0.0",
    properties = mapOf()
)
