//
//  CustomPlugin.h
//  ItlyObjCCarthageExample
//
//  Created by Ondrej Hrebicek on 11/18/20.
//

#import "Itly.h"

#ifndef CustomPlugin_h
#define CustomPlugin_h

@interface CustomPlugin : ITLPlugin

@end

#endif /* CustomPlugin_h */
