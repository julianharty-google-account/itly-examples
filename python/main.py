import multiprocessing
import itly

from custom import CustomDestination
from logger import SampleLogger

itly.load(itly.Options(
    context=itly.Context(
        version="1.0"
    ),
    logger=SampleLogger(),
    destinations=itly.DestinationsOptions(
        custom=itly.CustomOptions(
            adapter=CustomDestination()
        )
    )
))

print("Starting process...")

itly.identify("some-user-id", first_name="John", last_name="Doe")
itly.process_started("some-user-id",
    available_processors=multiprocessing.cpu_count()
)
itly.track("some-user-id", itly.ProcessStarted(
    available_processors=multiprocessing.cpu_count()
))

print("Ending process...")
