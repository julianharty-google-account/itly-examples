package ly.iterative.examples.kotlin

import ly.iterative.itly.*

class MyCustomDestination : Plugin("my-custom-destination") {
    override fun load(options: PluginLoadOptions) {
        println("MyCustomDestination.init()")
    }

    override fun alias(userId: String, previousId: String?) {
        println(
            String.format(
                "MyCustomDestination.alias(userId: %s, previousId: %s)",
                userId,
                previousId
            )
        )
    }

    override fun identify(userId: String?, properties: Properties?) {
        println(
            String.format(
                "MyCustomDestination.identify(userId: %s, properties: %s)",
                userId,
                properties.toString()
            )
        )
    }

    override fun group(userId: String?, groupId: String, properties: Properties?) {
        println(
            String.format(
                "MyCustomDestination.group(groupId: %s, properties: %s)",
                groupId,
                properties.toString()
            )
        )
    }

    override fun reset() {
        println("MyCustomDestination.reset()")
    }

    override fun track(userId: String?, event: Event) {
        println(
            String.format(
                "MyCustomDestination.track(eventName: %s, userId: %s, properties: %s)",
                event.name,
                userId,
                event.properties.toString()
            )
        )
    }
}
