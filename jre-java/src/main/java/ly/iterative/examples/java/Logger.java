package ly.iterative.examples.java;

import io.itly.*;

public class Logger extends ItlyLogger {
    public void debug(String message) {
        System.err.println(message);
    }
    public void info(String message) {
        System.out.println(message);
    }
    public void warn(String message) {
        System.out.println(message);
    }
    public void error(String message) {
        System.err.println(message);
    }
}
