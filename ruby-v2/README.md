# Iteratively Ruby (2.0) Sample Application

## Overview
This project is an example Ruby application using the strongly typed Ruby SDK generated
by Iteratively via `itly pull <ruby-v2-source>` for a Tracking Plan with the following Events:

* User Signed In
* User Signed Out
* Song Played

If you want to go straight to code `app.rb` is a great place to start.

## REQUIRED CHANGES
By default, this example only validates and logs Events to standard output.

If you want to see events in included destinations such as Amplitude, Mixpanel, Segment, Snowplow
you need to update the `load()` method in `itly/itly.rb`.

1. Uncomment the Plugins you want to activate
2. Set a valid API key for the given Plugin


## Project Files

* ruby-v2
    * app.rb - Sample app using the generated Itly SDK
    * custom_plugin.rb - Example Custom Plugin implementation
    * itly/itly.rb - The code generated Itly SDK for an example tracking plan. Created via `itly pull <ruby-v2-source>`.
    * Gemfile - Project dependencies
    * README.md - You are here *

# Setup

## Install a Ruby

You can install Ruby through your distribution's packages or through a Ruby Manager.

## Install a Ruby manager

The best option to run ruby on your computer is to use a Ruby manager such as rbenv or rvm.

Follow the instruction provided in the related repository:

- Rbenv: https://github.com/rbenv/rbenv
- RVM: https://rvm.io/rvm/install

Once the manager is installed, install the version of ruby you need.

## Install the gems

Then install the bundler gem:

    gem install bundler

And install the itly gems and their dependencies:

    cd /ruby-v2
    bundle install

## Run

To run the sample app:

    cd /ruby-v2
    bundle exec ruby app.rb
