package io.itly;

import java.util.HashMap;

public class Identify extends ItlyPropertyMap {
    private Identify() {}

    private Identify(Builder builder) {
        this.properties.putAll(builder.properties.properties);
    }

    private Identify(ItlyPropertyMap clone) {
        this.properties.putAll((HashMap<String, Object>) clone.properties.clone());
    }

    public Identify clone() {
        return new Identify(this);
    }

    public static IFirstName builder() { return new Builder(); }

    // Inner Builder class with required properties
    public static class Builder implements IFirstName, ILastName, IBuild {
        Identify properties = new Identify();

        private Builder() {}

        /**
         * The user's first name.
         * <p>
         * Must be followed by {@link ILastName#lastName(String)
         */
        public ILastName firstName(String firstName) {
            this.properties.properties.put("firstName", firstName);
            return this;
        }

        /**
         * The user's last name.
         * <p>
         * Must be followed by by additional optional properties or build() method
         */
        public Builder lastName(String lastName) {
            this.properties.properties.put("lastName", lastName);
            return this;
        }

        public Identify build() {
            return new Identify(this);
        }
    }

    // Required property interfaces
    public interface IFirstName {
        ILastName firstName(String firstName);
    }

    public interface ILastName {
        Builder lastName(String lastName);
    }

    /** Build interface with optional properties */
    public interface IBuild {
        Identify build();
    }
}
