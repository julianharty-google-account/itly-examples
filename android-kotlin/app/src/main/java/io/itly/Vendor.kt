//
//  Vendor.kt
//  This file is auto-generated by Iteratively. Run `itly pull kotlin` to update.
//

package io.itly

enum class Vendor {
    AMPLITUDE,
    MIXPANEL,
    SEGMENT,
    CUSTOM
}