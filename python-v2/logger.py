from __future__ import print_function
import sys

import itly


class SampleLogger(itly.Logger):
    _log_tag = "SampleLogger: "

    def debug(self, message: str) -> None:
        print(self._log_tag + message)

    def info(self, message: str) -> None:
        print(self._log_tag + message)

    def warn(self, message: str) -> None:
        print(self._log_tag + message)

    def error(self, message: str) -> None:
        print(self._log_tag + message, file=sys.stderr)