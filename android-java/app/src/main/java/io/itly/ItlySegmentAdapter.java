package io.itly;

import com.segment.analytics.Analytics;
import com.segment.analytics.Properties;
import com.segment.analytics.Traits;

import java.util.Iterator;
import java.util.Map;

public class ItlySegmentAdapter implements IItlyDestination {
    private ItlyOptions options;

    ItlySegmentAdapter(ItlyOptions options, String apiToken) {
        this.options = options;
        Analytics.Builder analyticsBuilder = new Analytics.Builder(options.destinations.segment.getAndroidContext(), apiToken);

        Analytics analytics = analyticsBuilder.build();
        Analytics.setSingletonInstance(analytics);
    }

    public void init() {
        // N/A for Segment
    }

    public void alias(String userId, String previousId) {
        // Segment keeps track of previousId internally, therefore it does not use @previousId
        Analytics.with(this.options.destinations.segment.getAndroidContext()).alias(userId);
    }

    public void identify(String userId, ItlyPropertyMap identifyProperties) {
        // convert properties to traits.
        Traits traits = null;
        if (identifyProperties != null && identifyProperties.getSize() > 0) {
            traits = new Traits();

            Iterator<Map.Entry<String, Object>> contextKeys = identifyProperties.getEntrySet().iterator();
            while (contextKeys.hasNext()) {
                Map.Entry<String, Object> property = contextKeys.next();
                traits.putValue(property.getKey(), property.getValue());
            }
        }

        if (traits == null) {
            Analytics.with(this.options.destinations.segment.getAndroidContext()).identify(userId);
        }
        else {
            Analytics.with(this.options.destinations.segment.getAndroidContext()).identify(userId, traits, null);
        }
    }

    public void group(String groupId, ItlyPropertyMap groupProperties) {
        Traits traits = null;
        if (groupProperties != null && groupProperties.getSize() > 0) {
            traits = new Traits();

            Iterator<Map.Entry<String, Object>> contextKeys = groupProperties.getEntrySet().iterator();
            while (contextKeys.hasNext()) {
                Map.Entry<String, Object> property = contextKeys.next();
                traits.putValue(property.getKey(), property.getValue());
            }
        }

        Analytics.with(this.options.destinations.segment.getAndroidContext()).group(groupId, traits, null);
    }

    public void track(String userId, String eventName, ItlyPropertyMap eventProperties) {
        Properties segmentProperties = new Properties();

        Iterator<Map.Entry<String, Object>> eventPropertyEntries = eventProperties.getEntrySet().iterator();
        while (eventPropertyEntries.hasNext()) {
            Map.Entry<String, Object> propertyEntry = eventPropertyEntries.next();
            segmentProperties.put(propertyEntry.getKey(), propertyEntry.getValue());
        }

        Analytics.with(this.options.destinations.segment.getAndroidContext()).track(eventName, segmentProperties, null);
    }

    public void reset() {
        Analytics.with(this.options.destinations.segment.getAndroidContext()).reset();
    }
}
