require 'itly'

Itly.load(
  context: Itly::Context.new(
    required_string: 'Ruby context string',
    optional_enum: Itly::Context::OptionalEnum::VALUE_1
  ),
  destinations: Itly::DestinationOptions.new(
    iteratively: Itly::Plugin::Iteratively::Options.new(
      batch_size: 1,
      max_retries: 2
    ),
    snowplow: Itly::Plugin::Snowplow::Options.new(
      endpoint: 'sp-dev.iterative.ly'
    ),
  ),
  options: Itly::Options.new(
    validation: Itly::Options::Validation::ERROR_ON_INVALID,
    logger: Itly::Loggers.std_out
  )
)
